package com.test.userservice.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.test.userservice.dto.OrderDTO;


@FeignClient(name = "order-servce")
//@FeignClient(value = "service", url = "http://localhost:8083")
public interface OrderClient {

	
	@GetMapping("/orders")
	public List<OrderDTO> getOrders();
}

package com.test.userservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.userservice.client.OrderClient;
import com.test.userservice.dto.OrderDTO;

@RestController
public class UserController {

	
	@Autowired
	private OrderClient orderClient;
	
	@GetMapping("/users")
	public List<OrderDTO> getUser() {
		
		return orderClient.getOrders();
	}
	
}
